﻿// ThreadsVsTasks
// Copyright (C) 2018 Dust in the Wind
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Diagnostics;
using DustInTheWind.ConsoleTools;
using DustInTheWind.ConsoleTools.Spinners;
using DustInTheWind.ThreadsVsTasks.Generators;

namespace DustInTheWind.ThreadsVsTasks
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            const int levelCount = 10;

            CustomConsole.WriteLine("This application will generate a binary tree in three different ways.");
            CustomConsole.WriteLine("===============================================================================");
            CustomConsole.WriteLine();

            CustomConsole.WriteLine("Tree deep level: {0}", levelCount);

            Pause.QuickDisplay("Press any key to begin...");

            SingleThreadTreeGenerator singleThreadTreeGenerator = new SingleThreadTreeGenerator(levelCount);
            GenerateTree(singleThreadTreeGenerator);

            Pause.QuickDisplay();

            MultiThreadTreeGenerator multiThreadTreeGenerator = new MultiThreadTreeGenerator(levelCount);
            GenerateTree(multiThreadTreeGenerator);

            Pause.QuickDisplay();

            MultiTaskTreeGenerator multiTaskTreeGenerator = new MultiTaskTreeGenerator(levelCount);
            GenerateTree(multiTaskTreeGenerator);

            Pause.QuickDisplay();
        }

        private static void GenerateTree(ITreeGenerator treeGenerator)
        {
            Node rootNode;
            TimeSpan elapsedTime;

            using (Spinner spinner = new Spinner())
            {
                spinner.MarginTop = 1;
                spinner.Label = new InlineText
                {
                    Text = string.Format("Generating nodes using '{0}'", treeGenerator.GetType().Name),
                    ForegroundColor = ConsoleColor.White,
                    MarginRight = 1
                };
                spinner.DoneText = new InlineText("[Done]", ConsoleColor.Green);
                spinner.Display();

                Stopwatch stopwatch = Stopwatch.StartNew();
                rootNode = treeGenerator.Generate();
                elapsedTime = stopwatch.Elapsed;
            }

            int nodeCount = CountNodes(rootNode);
            Console.WriteLine("Generated nodes: {0}", nodeCount);
            Console.WriteLine("Elapsed time: {0}", elapsedTime);
        }

        private static int CountNodes(Node node)
        {
            if (node == null)
                return 0;

            return 1 + CountNodes(node.LeftNode) + CountNodes(node.RightNode);
        }
    }
}